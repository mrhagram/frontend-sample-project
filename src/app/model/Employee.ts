
export class Employee{
    employeeId: number | undefined;
    fullname: string | undefined;
    age: number | undefined;
    gender: string | undefined;
}