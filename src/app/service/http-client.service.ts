import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../model/Employee';
@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  

  constructor(private httpClient:HttpClient) { }
  
  getEmployees(){
   
    return this.httpClient.get<Employee[]>("http://localhost:5000/api/v0/all");
  }
}
