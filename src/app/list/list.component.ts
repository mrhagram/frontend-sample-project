import { Component, OnInit } from '@angular/core';
import { Employee } from '../model/Employee';
import { HttpClientService } from '../service/http-client.service';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  employees : Array<Employee> | undefined;
  constructor(private httpService : HttpClientService) { }

  ngOnInit(): void {
    this.httpService.getEmployees().subscribe(employee => this.employees = employee);
  }
}